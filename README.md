# github.com/axiangcoding/axth

___
[Not Release Yet] A set of tool to make login and register easy!

[![codecov](https://codecov.io/gh/axiangcoding/axth/branch/master/graph/badge.svg?token=BGL1MBRJ2H)](https://codecov.io/gh/axiangcoding/axth)
[![CodeQL](https://github.com/axiangcoding/axth/actions/workflows/codeql-analysis.yml/badge.svg)](https://github.com/axiangcoding/axth/actions/workflows/codeql-analysis.yml)
[![Test](https://github.com/axiangcoding/axth/actions/workflows/test.yml/badge.svg)](https://github.com/axiangcoding/axth/actions/workflows/test.yml)
[![Total alerts](https://img.shields.io/lgtm/alerts/g/axiangcoding/axth.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/g/axiangcoding/axth/alerts/)
[![Language grade: Go](https://img.shields.io/lgtm/grade/go/g/axiangcoding/axth.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/g/axiangcoding/axth/context:go)